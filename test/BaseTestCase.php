<?php
namespace XimilarTest\Client;

use Jchook\AssertThrows\AssertThrows;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{

    //use AssertThrows; // <--- adds the assertThrows method

    protected function getToken(): string {
        return getenv('XIMILAR_AUTH_TOKEN');
    }

}