<?php

namespace Ximilar\Client;

function is_assoc($arr)
{
    return array_keys($arr) !== range(0, count($arr) - 1);
}

/**
 * Cleans up and prepares request parameters
 * @param array $params
 * @return array
 */
function prepareParams(array $params): array
{
    // If params given as array in the first argument
    if (!is_assoc($params) && count($params) == 1 && is_array($params[0])) {
        $params = $params[0];
    }
    return $params;
}
