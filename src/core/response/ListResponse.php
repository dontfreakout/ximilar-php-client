<?php


namespace Ximilar\Client\Response;

use Ximilar\Client\Model\XimilarModel;
use Ximilar\Client\Request\ListRequest;
use Ximilar\Client\Request\Request;
use Ximilar\Client\XimilarClient;
use function Ximilar\Client\is_assoc;

/**
 * Class ListResponse
 * Response from list request
 * @package Ximilar\Client\Response
 */
class ListResponse extends Response
{
    private int $count;
    private ?int $next = null;
    private ?int $prev = null;
    private array $results;

    public function __construct(XimilarClient $client, Request $request, array $rawResponse)
    {
        parent::__construct($client, $request, $rawResponse);

        if (is_assoc($rawResponse)) {
            $results = $rawResponse['results'];
            $this->count = $rawResponse['count'];

            $prev = $rawResponse['previous'];
            if ($prev) {
                $this->prev = $this->extractPage($prev);
            }
            $next = $rawResponse['next'];
            if ($next) {
                $this->next = $this->extractPage($next);
            }
        } else {
            // No paging happens..
            $results = $rawResponse;
            $this->count = count($rawResponse);
        }

        $this->results = $this->processResults($results);
    }

    /**
     * Returns list of XimilarModel instances
     * @param array $results
     * @return array
     */
    private function processResults(array $results): array
    {
        $modelClass = $this->request->getModelClass();
        return array_map(function ($data) use ($modelClass) {
            return XimilarModel::create($modelClass, $data, $this->client);
        }, $results);
    }

    /**
     * Extracts page number from given rest list url
     * @param string $url
     * @return int
     */
    private function extractPage(string $url): int
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $params);
        return (int)($params['page'] ?? 1);
    }

    /**
     * Returns next page
     * @return ListResponse|null
     * @throws \Ximilar\Client\Exception\XimilarClientException
     */
    public function next(): ?ListResponse
    {
        return $this->listPage($this->getNextPage());
    }

    /**
     * Returns next page
     * @return ListResponse|null
     * @throws \Ximilar\Client\Exception\XimilarClientException
     */
    public function previous(): ?ListResponse
    {
        return $this->listPage($this->getPreviousPage());
    }

    /**
     * @param int|null $page
     * @return ListResponse|null
     * @throws \Ximilar\Client\Exception\XimilarClientException
     */
    private function listPage(?int $page): ?ListResponse
    {
        if ($page) {
            return $this->client->list($this->request->clone(["page" => $page]));
        } else {
            return null;
        }
    }

    /**
     * @return int|mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getNextPage(): ?int
    {
        return $this->next;
    }

    /**
     * @return int
     */
    public function getPreviousPage(): ?int
    {
        return $this->prev;
    }

    /**
     * @return array|mixed
     */
    public function getResults()
    {
        return $this->results;
    }
}
