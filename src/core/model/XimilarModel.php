<?php


namespace Ximilar\Client\Model;

use Ximilar\Client\Exception\NotImplementedException;
use Ximilar\Client\Exception\XimilarClientException;
use Ximilar\Client\XimilarClient;

/**
 * @property mixed|null id
 */
abstract class XimilarModel
{

    protected const CMD_DELETE = "";
    protected const CMD_RETRIEVE = "";
    protected const CMD_PATCH = "";

    /**
     * Array containing the instance data
     * @var array
     */
    private array $_data;

    /**
     * Reference to related Client
     * @var XimilarClient
     */
    private XimilarClient $_client;

    /**
     * Contains keys of fields which are changed
     * @var array
     */
    private array $_changes;

    /**
     * If true, the instance exists only locally
     * @var bool
     */
    private bool $_shallow;

    public function __construct(array $data, XimilarClient $client)
    {
        $this->_data = $data;
        $this->_client = $client;
        $this->_changes = [];
        $this->_shallow = empty($this->id);
    }

    /**
     * Creates and returns an instance of the model
     * @param string $cls
     * @param array $data
     * @param XimilarClient $client
     * @return mixed
     */
    public static function create(string $cls, array $data, XimilarClient $client)
    {
        return new $cls($data, $client);
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_data)) {
            return $this->_data[$name];
        }
        return null;
    }

    /**
     * @param $name
     * @param $value
     * @throws XimilarClientException
     */
    public function __set($name, $value)
    {
        if(in_array($name, $this->getImmutableFields())) {
            throw new XimilarClientException("Cannot change an immutable field: $name");
        }
        $this->_data[$name] = $value;
        if(!in_array($name, $this->_changes)) {
            array_push($this->_changes, $name);
        }
    }

    public function __isset($name)
    {
        return isset($this->_data[$name]);
    }

    public function __unset($name)
    {
        unset($this->_data[$name]);
    }

    /**
     * Returns a list of immutable field names
     * @return array
     */
    public function getImmutableFields(): array {
        return ["id"];
    }

    /**
     * Returns true if the instance has some changes
     * @return bool
     */
    public function isDirty(): bool {
        return count($this->_changes) > 0;
    }

    /**
     * Returns array of dirty field names
     * @return array
     */
    protected function getDirtyFields(): array {
        $output_arr = [];
        foreach($this->_changes as $key) {
            $output_arr[$key] = $this->_data[$key];
        }
        return $output_arr;
    }

    /**
     * Saves the instance changed data
     * @throws NotImplementedException
     * @throws XimilarClientException
     */
    public function save()
    {
        if($this::CMD_PATCH) {
            $this->_client->execute($this::CMD_PATCH, array_merge(["id" => $this->id], $this->getDirtyFields()));
            $this->_changes = [];
        } else {
            throw new NotImplementedException();
        }
    }

    /**
     * Deletes the instance
     * @throws XimilarClientException
     * @throws NotImplementedException
     */
    public function delete()
    {
        if($this::CMD_DELETE) {
            $this->_client->execute($this::CMD_DELETE, ["id" => $this->id]);
            $this->_shallow = true;
        } else {
            throw new NotImplementedException();
        }
    }

    /**
     * Reloads the instance from the API
     * @throws NotImplementedException
     * @throws XimilarClientException
     */
    public function reload()
    {
        if($this->_shallow) {
            throw new XimilarClientException("Cannot reload a shallow instance.");
        }
        if($this::CMD_RETRIEVE) {
            $this->_data = $this->_client->execute($this::CMD_RETRIEVE, ["id" => $this->id]);
            $this->_changes = [];
        } else {
            throw new NotImplementedException();
        }
    }
}
