<?php

namespace Ximilar\Client\Recognition\Model;


use Ximilar\Client\Model\XimilarModel;
use const Ximilar\Client\Recognition\CMD_RECOGNITION_LABEL_DELETE;
use const Ximilar\Client\Recognition\CMD_RECOGNITION_LABEL_PATCH;
use const Ximilar\Client\Recognition\CMD_RECOGNITION_LABEL_RETRIEVE;

/**
 * @property string|null description
 * @property string name
 */
class RecognitionLabel extends XimilarModel
{
    const CMD_DELETE = CMD_RECOGNITION_LABEL_DELETE;
    const CMD_RETRIEVE = CMD_RECOGNITION_LABEL_RETRIEVE;
    const CMD_PATCH = CMD_RECOGNITION_LABEL_PATCH;
}
